# Libracmp #

Simple tool for doing elaborate (configurable) comparison of (structured) data. Mainly focused on basic data types, lists, and dicts, and their combinations.
Behavior of the comparison can be adjusted either by mode (global) or by filters (index/key specific). 

### Benefits/Features ###

1. int(1) == float(1.0), and you might want to prevent that. (TypeStrictComparator)
1. 'anything' != None, for some reason you don't care when one of the parameter is None (e.g. missing) (RelaxedComparator, MODE_RELAXED)
1. Comparison difference, organized by path (index/key) for structured data

### Compatibility ###

Currently project is only compatible with python 2.7.
Compatibility through __future__ module will be added in the future.

### Installation ###

1. Clone this repo
1. cd libracmp
1. pip install [--user] .

or (when available)

1. pip install libracmp

### Basic Usage ###
```
#!python

import libracmp
from libracmp import Comparator

a = 1
b = 2

comp = Comparator(a, b)

print comp.result
print comp.diff

```

### Advanced usage ###

#### Modes ####

There is a 'mode' parameter in the Comparator, it is a dictionary with specific comparator settings for specific occasion.

So it chooses a specific *Comparator to a specific values. For list values (basic data types) it uses a 'default' setting from a mode, for a list is uses a 'list', and for dict is uses a 'dict' setting (how original).

There are few predefined modes you should used instead of creating your own. You can find them in module 'libracmp':

* **MODE_STRICT** (**MODE_DEFAULT**) - this is the default as for libracmp, be aware of that. Messaging default is set by MessagingProcessor so don't be afraid. This performs python '=='  comparison, so for example int(1) and float(1.0) equals.
* **MODE_RELAXED** - this is the messaging default, if you will use comparator through MessagingProcessor, as you should in a first place, the relaxed default is set there. Performs normal python comparison as above, with exception of one or both operands are none, then it returns True, as it relaxes if some of them are not available.
* **MODE_IGNORE** - just ignores all comparison by default. Why it is useful you ask? For example, when you want to compare only specific keys, maybe just one, and disregard the others as they might be prone to change. (aka the inclusion filtering)
* **MODE_TYPESTRICT** - before performing regular comparison, check the types, and it must match so int(1) vs. float(1.0) -> False, 'x' vs. u'x' -> False

```
#!python

comp = Comparator(a, b, mode=libracmp.MODE_RELAXED)
```

#### Filters ####

So filters are just an overlay onto a mode. They define a specific comparator to be used for specific key paths.

If that specific key is reached in comparison, then filter defined comparator is used instead of mode one.

Keep in mind that correct path has to be defined. For that there is a specific key object prepared in module 'libracmp'. libracmp.All() if this instance (it is a singleton so don't be afraid) is used as a key, it defines a 'default' path for all non specified keys on that level. So if you have fitler {libracmp.All(): XComp, 'y': YComp} then for value in key 'y' -> YComp will be used, and XComp for others. There is no filter merging magic. All maybe better represented by word default (again) but All is shorter and it is more like a wildcard, because of that -- All().


Let's have a nice messaging example:

```
#!python

a = [{'durable': True, 'count': 0, 'priority': 4, 
      'custom': {'count': 0}, 
      'properties': {'my_property': 'test' 'count': 0}}]
b = [{'durable': True, 'count': 1, 'priority': 6, 
      'custom': {'custom': 2}, 
      'properties': {'my_property': 'test' 'custom': 3}}]
```

let's assume MODE_RELAXED as messaging default.

To ignore differences here (if applicable) would be written like:

```
#!python

filters = {  # list level
  libracmp.All(): { # base level, this ensure applying the filter to all items in the list
    'count': IgnoreComparator,
    'priority': IgnoreComparator,
    'custom': {  # additional level for custom 
      'count': IgnoreComparator  # only this value from custom
    },
    'properties': {  # same as custom
      'count': IgnoreComparator
      # here 'my_property' is not specified, 
      # and thus RelaxedComparator from MODE_RELAXED will be used
    }  
  }
}
```

NOTE: that if you need to ignore all 'properties' for example, there is no need write ignore specifically for every item:

```
#!python

..., 
'properties': {
  'my_property': IgnoreComparator, 
  'count': IgnoreComparator, 
  'something': IgnoreComparator, 
  ... }, 
... # this is OVERLY COMPLICATED, DO NOT USE
```
 

neither you don't have to use wildcard key All() there:

```
#!python

..., 'properties': {libracmp.All(): IgnoreComparator}, .... 
# this is just COMPLICATED, DO NOT USE
```
 

But very simply apply IgnoreComparator to properties on upper level:

```
#!python

..., 'properties': IgnoreComparator, .... # this is CORRECT, just ignore the whole key
```
 


### Contribution guidelines ###

If you would like to contribute, keep in mind, that any new additions should be generic, if you need to add specific behavior to you application, better way would be to inherit existing and add your behavior to your project.


### Contributors ###

* Zdenek Kraus <zdenek.kraus@gmail.com>

### License ###

Copyright 2017 Zdenek Kraus <zdenek.kraus@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.